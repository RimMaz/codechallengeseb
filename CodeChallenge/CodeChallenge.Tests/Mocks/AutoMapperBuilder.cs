﻿using AutoMapper;
using CodeChallenge.Core.Automapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeChallenge.Tests.Mocks
{
    public class AutoMapperBuilder
    {
        private readonly MapperConfiguration _mappingConfiguration;

        public AutoMapperBuilder()
        {
            _mappingConfiguration = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new CutomerProfile());
                mc.AddProfile(new AgreementProfile());
            });
        }

        public IMapper BuildMapper()
        {
            return _mappingConfiguration.CreateMapper();
        }
    }
}
