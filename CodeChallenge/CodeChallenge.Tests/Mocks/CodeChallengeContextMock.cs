﻿using CodeChallenge.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeChallenge.Tests.Mocks
{
    public class CodeChallengeContextMock : CodeChallengeContext
    {
        public CodeChallengeContextMock(DbContextOptionsBuilder<CodeChallengeContext> builder)
          : base(builder.UseInMemoryDatabase(Guid.NewGuid().ToString())
              .EnableSensitiveDataLogging()
              .ConfigureWarnings(a => a.Ignore(InMemoryEventId.TransactionIgnoredWarning))
              .Options)
        {
            FillModel();
        }

        public void FillModel()
        {
            DbInitializer.Initialize(this);
        }
    }
}
