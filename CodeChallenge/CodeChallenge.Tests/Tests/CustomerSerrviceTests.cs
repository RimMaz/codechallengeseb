﻿using AutoMapper;
using CodeChallenge.Core.Automapper;
using CodeChallenge.Core.Interfaces.Repositories;
using CodeChallenge.Core.Interfaces.Services;
using CodeChallenge.Core.Interfaces.WebServices;
using CodeChallenge.Core.Models;
using CodeChallenge.Data.Context;
using CodeChallenge.Data.Repositories;
using CodeChallenge.Services.Services;
using CodeChallenge.Services.WebServices;
using CodeChallenge.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CodeChallenge.Tests
{
    [TestClass]
    public class CustomerSerrviceTests
    {
        private readonly IOptions<AppSettingsModel> _config;
        private readonly IVilibidViliborService _webService;
        private readonly ICustomerService _service;
        private readonly ICustomerRepository _repository;
        private readonly CodeChallengeContext _context;
        private readonly IMapper _mapper;

        public CustomerSerrviceTests()
        {
            _context = new CodeChallengeContextMock(new DbContextOptionsBuilder<CodeChallengeContext>());
            _repository = new CustomerRepository(_context);
            _service = new CustomerService(_repository);           
            _mapper = new AutoMapperBuilder().BuildMapper();

            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", false)
               .Build();
            _config = Options.Create(configuration.GetSection("AppSettings").Get<AppSettingsModel>());
            _webService = new VilibidViliborService(_config);

            AutoMapperConfig.SetUpConfiguration();
        }

        [TestMethod]
        public void GetCustomersAsync_ReturnsCustomers()
        {
            //Arrange
            var expected = _context.Customers.Select(o => _mapper.Map<CustomerModel>(o)).ToListAsync().Result;

            //Act
            var actual = _service.GetCustomersAsync().Result;

            //Assert
            //Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected.Count, actual.Count);
        }

        [TestMethod]
        public void GetCustomerAgreementsAsync_ReturnsCustomersAgreements()
        {
            //Arrange
            var expectedCustomer = _context.Customers.FirstOrDefault();
            var expected = _context.Customers
                .Where(o => o.PersonalId == expectedCustomer.PersonalId)
                .Include("Agreements")
                .FirstAsync().Result; 

            //Act
            var actual = _service.GetCustomerAgreementsAsync(expectedCustomer.PersonalId).Result;

            //Assert
            Assert.AreEqual(expected.PersonalId, actual.PersonalId);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Surname, actual.Surname);
            Assert.AreEqual(expected.Agreements.Count, actual.Agreements.Count);
        }

        [TestMethod]
        public void GetBaseRateCodesAsync_ReturnsBaseCodes()
        {
            //Arrange
            var expected = _context.BaseRateCodes.ToListAsync().Result;

            //Act
            var actual = _service.GetBaseRateCodesAsync().Result;

            //Assert
            Assert.AreEqual(expected.Count, actual.Count);
        }

        [TestMethod]
        public void GetLatestBaseRateAsync_ReturnsLatestBaseRate()
        {
            //Arrange
            var expectedBaseCode = _context.BaseRateCodes.FirstOrDefault();

            //Act
            var actual = _webService.GetLatestBaseRateAsync(expectedBaseCode.Code).Result;

            //Assert
            Assert.IsTrue(actual > 0);
        }

        [TestMethod]
        public void GetLatestBaseRateAsync_WrongBaseRateCode_ReturnsMinusOne()
        {
            //Act
            var actual = _webService.GetLatestBaseRateAsync("test").Result;

            //Assert
            Assert.IsFalse(actual > 0);
        }
    }
}
