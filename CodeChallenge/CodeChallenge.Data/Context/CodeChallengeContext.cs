﻿using CodeChallenge.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CodeChallenge.Data.Context
{
    public class CodeChallengeContext : DbContext
    {
        public CodeChallengeContext(DbContextOptions<CodeChallengeContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Agreement> Agreements { get; set; }
        public DbSet<BaseRateCode> BaseRateCodes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().ToTable("Customers");
            modelBuilder.Entity<Agreement>().ToTable("Agreements");
            modelBuilder.Entity<BaseRateCode>().ToTable("BaseRateCodes");
        }
    }
}
