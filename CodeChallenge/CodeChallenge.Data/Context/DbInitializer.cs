﻿using System.Linq;
using CodeChallenge.Core.Entities;

namespace CodeChallenge.Data.Context
{
    public static class DbInitializer
    {
        public static void Initialize(CodeChallengeContext context)
        {
            context.Database.EnsureCreated();

            if (context.Customers.Any())
            {
                return;   // Db is created
            }

            var baseRateCodes = new BaseRateCode[]
            {
                new BaseRateCode(){ Code = "VILIBOR1m"},
                new BaseRateCode(){ Code = "VILIBOR3m"},
                new BaseRateCode(){ Code = "VILIBOR6m"},
                new BaseRateCode(){ Code = "VILIBOR1y"}
            };

            foreach (var baseRate in baseRateCodes)
            {
                context.BaseRateCodes.Add(baseRate);
            }

            var gorasAgreements = new Agreement[]
            {
                new Agreement(){Amount = 12000, BaseRateCode = "VILIBOR3m", Margin = 1.6, Duration = 60}
            };

            var dangeAgreements = new Agreement[]
            {
                new Agreement(){Amount = 8000, BaseRateCode = "VILIBOR1y", Margin = 2.2, Duration = 36},
                new Agreement(){Amount = 1000, BaseRateCode = "VILIBOR6m", Margin = 1.85, Duration = 24}

             };

            var customers = new Customer[]
            {
                new Customer(){ PersonalId = "67812203006", Name = "Goras", Surname = "Trusevičius", Agreements = gorasAgreements},
                new Customer(){ PersonalId = "78706151287", Name = "Dange", Surname = "Kulkavičiūtė", Agreements = dangeAgreements}
            };

            foreach (var customer in customers)
            {
                context.Customers.Add(customer);
            }

            context.SaveChanges();            
        }
    }
}