﻿using CodeChallenge.Data.Context;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using CodeChallenge.Core.Entities;
using CodeChallenge.Core.Interfaces.Repositories;

namespace CodeChallenge.Data.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private CodeChallengeContext _context;

        public CustomerRepository(CodeChallengeContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Customer>> GetCustomersAsync()
        {
            return await _context.Customers.ToListAsync();
        }

        public async Task<Customer> GetCustomerAgreementsAsync(string personalId)
        {
            return await _context.Customers
                .Where(o => o.PersonalId == personalId)
                .Include("Agreements")
                .FirstOrDefaultAsync();                
        }

        public async Task<IEnumerable<BaseRateCode>> GetBaseRateCodesAsync()
        {
            return await _context.BaseRateCodes.ToListAsync();
        }
    }
}
