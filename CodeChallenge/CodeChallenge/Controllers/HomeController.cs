﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeChallenge.Core.Interfaces.Services;
using CodeChallenge.Core.Interfaces.WebServices;
using System.Net.Http;

namespace CodeChallenge.Controllers
{
    [Produces("application/json")]
    [Route("Home")]
    public class HomeController : Controller
    {
        private readonly IVilibidViliborService _webService;
        private readonly ICustomerService _customerSerice;

        public HomeController(ICustomerService customerService,
            IVilibidViliborService webService)
        {
            _customerSerice = customerService;
            _webService = webService;
        }

        [HttpGet("Swagger")]
        public IActionResult Swagger()
        {
            return new RedirectResult("~/swagger");
        }

        [HttpGet("InterestRate")]
        public IActionResult InterestRate()
        {
            return View();
        }

        [HttpGet("GetCustomerNames")]
        public async Task<IActionResult> GetCustomerNames()
        {
            var customerNames = await _customerSerice.GetCustomersAsync();
            return Ok(customerNames);
        }

        [HttpGet("GetCustomerAndAgreements/{customerId}")]
        public async Task<IActionResult> GetCustomerAndAgreements(string customerId)
        {
            if(customerId == null)
            {
                return BadRequest();
            }
            var customer = await _customerSerice.GetCustomerAgreementsAsync(customerId);
            return Ok(customer);
        }

        [HttpGet("GetBaseRateCodes")]
        public async Task<IActionResult> GetBaseRateCodes()
        {
            var baseRateCodes = await _customerSerice.GetBaseRateCodesAsync();
            return Ok(baseRateCodes);
        }

        [HttpGet("GetLatestBaseRateByCode")]
        public async Task<IActionResult> GetLatestBaseRateByCode(string baseRateCode)
        {
            if (baseRateCode == null)
            {
                return BadRequest();
            }
            try
            {
                var baseRate = await _webService.GetLatestBaseRateAsync(baseRateCode);
                if(baseRate == -1)
                {
                    return StatusCode(500);
                }
                return Ok(baseRate);
            }
            catch (HttpRequestException)
            {
                ViewData["Error"] = "WebService is unavailable";
                return StatusCode(500);
            }          
        }
    }
}
