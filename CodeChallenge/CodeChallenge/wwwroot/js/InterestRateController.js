﻿angular.module('InterestRate', [])
    .controller('InterestRateCtrl', InterestRateCtrl);

InterestRateCtrl.$inject = ['$scope', '$http'];
function InterestRateCtrl($scope, $http) {

    $scope.selectedCustomer = null;
    $scope.selectedAgreement = null;
    $scope.selectedNewBaseRate = null;

    $scope.currentInterestRate = null;
    $scope.newInterestRate = null;

    $scope.showCustomerDetails = false;
    $scope.showAgreementDetails = false;
    $scope.showInterestRatesDetails = false;

    $scope.showAgreementsSelect = false;
    $scope.showBaseRateCodesSelect = false;

    $scope.init = function () {
        GetCustomerNames();
        GetBaseRateCodes();
    };

    function GetCustomerNames() {
        $http.get('GetCustomerNames').then(function (result) {
            $scope.customers = result.data;
        },
        function (error) {
            $scope.error = error;
        });
    }

    function GetBaseRateCodes() {
        $http.get('GetBaseRateCodes').then(function (result) {
            $scope.baseRateCodes = result.data;
        },
        function (error) {
            $scope.error = error;
        });
    }

    function GetLatestBaseRate(baseRateCode) {
        var request = $http.get("GetLatestBaseRateByCode?baseRateCode=" + baseRateCode).then(function (result) {
            return result.data;
        },
        function (error) {
            $scope.error = error;
        });
        return request;
    }

    $scope.customerSelected = function () {

        if ($scope.selectedCustomer === undefined || $scope.selectedCustomer === null) {
            $scope.showCustomerDetails = false;
            $scope.showAgreementDetails = false;
            $scope.showInterestRatesDetails = false;
            $scope.showAgreementsSelect = false;
            $scope.showBaseRateCodesSelect = false;
            return;
        }

        $http.get("GetCustomerAndAgreements/" + $scope.selectedCustomer.personalId).then(function (result) {
            $scope.customer = result.data;

            // Show / Hide               
            $scope.showCustomerDetails = true;
            $scope.showAgreementDetails = false;
            $scope.showInterestRatesDetails = false;

            $scope.showAgreementsSelect = true;
            $scope.showBaseRateCodesSelect = false;
        },
            function (error) {
                $scope.error = error;
            });
    };

    $scope.agreementSelected = function () {

        if ($scope.selectedAgreement === undefined || $scope.selectedAgreement === null) {
            $scope.showAgreementDetails = false;
            $scope.showInterestRatesDetails = false;
            $scope.showBaseRateCodesSelect = false;
            return;
        }

        var promise = GetLatestBaseRate($scope.selectedAgreement.baseRateCode);
        promise.then(function (result) {
            var currentBaseRate = result;
            var calculatedInterestRate = CalculateInterestRate(currentBaseRate);
            $scope.currentInterestRate = RoundNumberToThreeDecimalPlaces(calculatedInterestRate);

            $scope.newBaseRateSelected();

            // Show / Hide                
            $scope.showAgreementDetails = true;
            $scope.showBaseRateCodesSelect = true;
        });
    };

    $scope.newBaseRateSelected = function () {

        if ($scope.selectedNewBaseRate === undefined || $scope.selectedNewBaseRate === null) {
            $scope.showInterestRatesDetails = false;
            return;
        }

        var promise = GetLatestBaseRate($scope.selectedNewBaseRate.code);
        promise.then(function (result) {
            var newBaseRate = result;
            var calcualtedInterestRate = CalculateInterestRate(newBaseRate);
            $scope.newInterestRate = RoundNumberToThreeDecimalPlaces(calcualtedInterestRate);
            $scope.InterestRateDifference = CalculateInterestRateDifference($scope.currentInterestRate, $scope.newInterestRate);

            // Show / Hide               
            $scope.showInterestRatesDetails = true;
        });
    };

    function CalculateInterestRate(baseRate) {
        return $scope.selectedAgreement.margin + baseRate;
    }

    function CalculateInterestRateDifference(oldInterest, newInterest) {
        var result = null;
        var difference = newInterest - oldInterest;
        if (difference > 0) {
            result = "+" + RoundNumberToThreeDecimalPlaces(difference);
        }
        else {
            result = RoundNumberToThreeDecimalPlaces(difference);
        }
        return result;
    }

    function RoundNumberToThreeDecimalPlaces(number) {
        return Math.round(number * 1000) / 1000;
    }
}