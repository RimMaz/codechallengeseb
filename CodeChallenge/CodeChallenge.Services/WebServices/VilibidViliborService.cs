﻿using CodeChallenge.Core.Interfaces.WebServices;
using CodeChallenge.Core.Models;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CodeChallenge.Services.WebServices
{
    public class VilibidViliborService : IVilibidViliborService
    {
        private readonly IOptions<AppSettingsModel> _config;

        public VilibidViliborService(IOptions<AppSettingsModel> config)
        {
            _config = config;
        }

        public async Task<double> GetLatestBaseRateAsync(string baseRate)
        {
            string baseUrl = _config.Value.VilibidBaseUrl;
            string webService = _config.Value.VilibidUrl + baseRate;

            using (var client = new HttpClient())
            {                
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                var response = await client.GetAsync(webService);
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                var xml = XDocument.Parse(stringResult);

                double latestBaseRate;
                if (!double.TryParse(xml.Root.Value, out latestBaseRate))
                {
                    return -1;
                };
                return latestBaseRate;                           
            }
        }
    }
}
