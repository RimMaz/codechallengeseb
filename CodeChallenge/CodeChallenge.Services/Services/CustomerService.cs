﻿using CodeChallenge.Core.Models;
using CodeChallenge.Core.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CodeChallenge.Core.Interfaces.Services;

namespace CodeChallenge.Services.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _repository;

        public CustomerService(ICustomerRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<CustomerModel>> GetCustomersAsync()
        {
            var customers = await _repository.GetCustomersAsync();
            return customers.Select(o => new CustomerModel()
            {
                PersonalId = o.PersonalId,
                Name = o.Name,
                Surname = o.Surname
            }).ToList();
        }

        public async Task<CustomerModel> GetCustomerAgreementsAsync(string customerId)
        {
            var customer = await _repository.GetCustomerAgreementsAsync(customerId);
            return Mapper.Map<CustomerModel>(customer);
        }

        public async Task<List<BaseRateCodeModel>> GetBaseRateCodesAsync()
        {
            var baseRateCodes = await _repository.GetBaseRateCodesAsync();
            return baseRateCodes.Select(o => Mapper.Map<BaseRateCodeModel>(o)).ToList();
        }   
    }
}
