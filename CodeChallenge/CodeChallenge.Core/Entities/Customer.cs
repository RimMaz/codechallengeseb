﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeChallenge.Core.Entities
{
    public class Customer
    {
        [Key]
        public string PersonalId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public virtual ICollection<Agreement> Agreements { get; set; }
    }
}
