﻿using System.ComponentModel.DataAnnotations;

namespace CodeChallenge.Core.Entities
{
    public class BaseRateCode
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
    }
}
