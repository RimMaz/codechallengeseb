﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeChallenge.Core.Entities
{
    public class Agreement
    {
        [Key]
        public int Id { get; set; }
        public double Amount { get; set; }
        public double Margin { get; set; }
        public string BaseRateCode { get; set; }
        public int Duration { get; set; }

        [ForeignKey("Customer")]
        public string CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

    }
}
