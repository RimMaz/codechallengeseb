﻿namespace CodeChallenge.Core.Models
{
    public class AgreementModel
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public double Margin { get; set; }
        public string BaseRateCode { get; set; }
        public int Duration { get; set; }
    }
}
