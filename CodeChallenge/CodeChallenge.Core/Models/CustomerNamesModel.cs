﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace CodeChallenge.Core.Models
{
    public class CustomerNamesModel
    {
        public List<SelectListItem> Customers { get; set; }
    }
}
