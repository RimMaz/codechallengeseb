﻿using System.Collections.Generic;

namespace CodeChallenge.Core.Models
{
    public class CustomerModel
    {
        public string PersonalId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<AgreementModel> Agreements { get; set; }
    }
}
