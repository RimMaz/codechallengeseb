﻿namespace CodeChallenge.Core.Models
{
    public class BaseRateCodeModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}
