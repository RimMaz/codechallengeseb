﻿namespace CodeChallenge.Core.Models
{
    public class AppSettingsModel
    {
        public string VilibidBaseUrl { get; set; }
        public string VilibidUrl { get; set; }
    }
}
