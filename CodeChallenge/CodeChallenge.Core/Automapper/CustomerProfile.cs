﻿using AutoMapper;
using CodeChallenge.Core.Entities;
using CodeChallenge.Core.Models;

namespace CodeChallenge.Core.Automapper
{
    public class CutomerProfile : Profile
    {
        public CutomerProfile()
        {
            CreateMap<Customer, CustomerModel>();
            CreateMap<CustomerModel, Customer>();
        }
    }
}
