﻿using AutoMapper;
using CodeChallenge.Core.Entities;
using CodeChallenge.Core.Models;

namespace CodeChallenge.Core.Automapper
{
    public class AgreementProfile : Profile
    {
        public AgreementProfile()
        {
            CreateMap<Agreement, AgreementModel>();
            CreateMap<AgreementModel, Agreement>();

            CreateMap<BaseRateCode, BaseRateCodeModel>();
            CreateMap<BaseRateCodeModel, BaseRateCode>();
        }
    }
}
