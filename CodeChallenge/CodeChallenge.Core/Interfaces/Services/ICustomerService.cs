﻿using CodeChallenge.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeChallenge.Core.Interfaces.Services
{
    public interface ICustomerService
    {
        Task<List<CustomerModel>> GetCustomersAsync();
        Task<CustomerModel> GetCustomerAgreementsAsync(string customerId);

        Task<List<BaseRateCodeModel>> GetBaseRateCodesAsync();
    }
}
