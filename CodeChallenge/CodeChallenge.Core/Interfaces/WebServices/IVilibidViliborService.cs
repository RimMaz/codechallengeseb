﻿using System.Threading.Tasks;

namespace CodeChallenge.Core.Interfaces.WebServices
{
    public interface IVilibidViliborService
    {
        Task<double> GetLatestBaseRateAsync(string baseRate);
    }
}
