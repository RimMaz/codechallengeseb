﻿using CodeChallenge.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeChallenge.Core.Interfaces.Repositories
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<Customer>> GetCustomersAsync();
        Task<Customer> GetCustomerAgreementsAsync(string personalId);

        Task<IEnumerable<BaseRateCode>> GetBaseRateCodesAsync();
    }
}
